<?php

namespace YB\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    public function loginAction()
    {
        return $this->render('AdminBundle::login.html.twig');
    }
}
